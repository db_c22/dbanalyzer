<%--
    Document   : index.jsp
    Created on : 10-Aug-2018, 12:53:18
    Author     : Ionut Burechita
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<jsp:useBean id="globalHelper" class="deutschebank.core.ApplicationScopeHelper" scope="application"/>

<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link rel="stylesheet"
              href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"
              integrity="sha384-BVYiiSIFeK1dGmJRAkycuHAHRg32OmUcww7on3RYdg4Va+PmSTsz/K68vbdEjh4u"
              crossorigin="anonymous">
        <link rel="stylesheet" type="text/css" href="dbanalyzer/css/main.css" />
        <script
            src="https://ajax.googleapis.com/ajax/libs/jquery/1.10.2/jquery.min.js">
        </script>
        <script
            src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"
            integrity="sha384-Tc5IQib027qvyjSMfHjOMaLkfuWVxZxUPnCJA7l2mCWNIpG9mGCD8wGNIcPD7Txa"
            crossorigin="anonymous">
        </script>
        <script src="dbanalyzer/js/main.js"></script>

        <title>Deutsche Bank Data Analyzer 2.2</title>
    </head>
    


    <body id="loginPageBody">
    <!-- Modal -->

        <%
            String  dbStatus = "The server is not responding. Please try again!";

            globalHelper.setInfo("Set any value here for application level access");
            boolean connectionStatus = globalHelper.bootstrapDBConnection();

            if( connectionStatus )
            {
                dbStatus = "The connection to Deutsche Bank server is stable!";
            }
        %>

        <div class="container welcome-container"  >
			<!--  <h1 class="form-heading">login Form</h1>-->
		<div class="row">
		  <div class="col-md-6">
		  	<div class="welcome-panel">
		  		<h1 class="welcome-title">Welcome to DB Analyzer</h1>
		  		<h3 class="welcome-subtitle"> A powerful data visualisation app</h3>
		  		<h3 class="welcome-subtitle"> We are on production!Yaaay!</h3>
		  		<button type="button" 
		  				class="btn connection-btn" 
		  				data-toggle="modal" 
		  				data-target="#myModal"
		  				>
		  		Check connection
		  		</button>


		  	</div>
		  	
		  </div>

		  <div class="col-md-6">
			<div class="login-form">
				<div class="main-div">
				    <div class="panel">
				    	<img class="db-logo" src="./dbanalyzer/images/logo.png"/>
				    	<h2>Sign in</h2>
				   		<p id="login-status-msg">Please enter your username and password</p>
				   	</div>
				   	
				    <form id="loginForm">
				        <div class="form-group">
				            <input type="text" class="form-control" name="usr" id="inputEmail" placeholder="Username" 
				            value="selvyn">
				        </div>

				        <div class="form-group">
				            <input type="password" class="form-control" name="pwd" id="inputPassword" placeholder="Password"
				            value="gradprog2016">
				        </div>
				        
				   	

				        <button type="submit" class="btn btn-primary">Login</button>

				    </form>
				</div>
			</div>
		</div>
		</div>
		</div>
		

	  <div class="modal fade" id="myModal" role="dialog">
	    <div class="modal-dialog">
	    
	      <!-- Modal content-->
	      <div class="modal-content">
	      <% if(connectionStatus){ %>
	        <div class="modal-header green-status" >
	      <% } else { %>
	      	<div class="modal-header red-status" >
	      <% } %>
	          <button type="button" class="close" data-dismiss="modal">&times;</button>
	          <h3 class="modal-title">Connection status</h3>
	        </div>
	        <div class="modal-body">
	          <h4><%=dbStatus %></h4>
	        </div>
	        
	        <div class="modal-footer">
	          <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	        
	        <% if(!connectionStatus){ %>
	          <button type="button" class="btn btn-primary" onclick="window.location.reload()">Reconnect</button>
	        <% } %>
	        
	        </div>
	      </div>
	      
	    </div>
	  </div>
	  </div>

    </body>
</html>
