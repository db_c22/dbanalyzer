 
var rootURL = "rws/services";
 
 
function getInstruments() {
	console.log("getInstruments");
	$.ajax(
            {
		method: "GET",
		url: rootURL + "/getInstruments",
		dataType: "json", // data type of response
                
		success : function(instrumentsJson) 
                {
//                    console.log(instrumentsJson);
                    CreateTableFromJSON(instrumentsJson)
		}
            }
        );
}


function getCounterparties() {
	console.log("getCounterparties");
	$.ajax(
            {
		method: "GET",
		url: rootURL + "/getCounterparties",
		dataType: "json", // data type of response
                
		success : function(counterpartiesJson) 
                {
                    console.log(counterpartiesJson);
                    CreateTableFromJSON(counterpartiesJson)
		}
            }
        );
}


function getDeals() {
	console.log("getDeals");
	$.ajax(
            {
		method: "GET",
		url: rootURL + "/getDeals",
		dataType: "json", // data type of response
                
		success : function(dealsJson) 
                {
//                    console.log(dealsJson);
                    CreateTableFromJSON(dealsJson)
		}
            }
        );
}


function CreateTableFromJSON(jsonRows) {

//        var jsonRows = JSON.parse (jsonString);

        // EXTRACT VALUE FOR HTML HEADER. 
        // ('Book ID', 'Book Name', 'Category' and 'Price')
        var col = [];
        for (var i = 0; i < jsonRows.length; i++) {
            for (var key in jsonRows[i]) {
                if (col.indexOf(key) === -1) {
                    col.push(key);
                }
            }
        }

        // CREATE DYNAMIC TABLE.
        var table = document.createElement("table");
        table.className = "table table-striped";
       

        // CREATE HTML TABLE HEADER ROW USING THE EXTRACTED HEADERS ABOVE.

        var tr = table.insertRow(-1);                   // TABLE ROW.

        for (var i = 0; i < col.length; i++) {
            var th = document.createElement("th");      // TABLE HEADER.
            th.innerHTML = col[i];
            tr.appendChild(th);
        }

        // ADD JSON DATA TO THE TABLE AS ROWS.
        for (var i = 0; i < jsonRows.length; i++) {

            tr = table.insertRow(-1);

            for (var j = 0; j < col.length; j++) {
                var tabCell = tr.insertCell(-1);
                tabCell.innerHTML = jsonRows[i][col[j]];
            }
        }

        // FINALLY ADD THE NEWLY CREATED TABLE WITH JSON DATA TO A CONTAINER.
        var divContainer = document.getElementById("showData");
        divContainer.innerHTML = "";
        divContainer.appendChild(table);
    }
